
install(FILES
        amgbackend.hh
        amgparallelhelpers.hh
        amgproperties.hh
        linearsolverproperties.hh
        seqsolverbackend.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/linear)
