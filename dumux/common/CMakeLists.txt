
install(FILES
        basicproperties.hh
        boundaryconditions.hh
        boundarytypes.hh
        boundingboxtree.hh
        defaultusagemessage.hh
        dumuxmessage.hh
        exceptions.hh
        geometrycollision.hh
        math.hh
        parameterparser.hh
        parameters.hh
        pointsource.hh
        propertysystem.hh
        start.hh
        timemanager.hh
        valgrind.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/common)
