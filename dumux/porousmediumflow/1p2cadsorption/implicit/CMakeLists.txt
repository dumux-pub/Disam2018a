
install(FILES
        fluxvariables.hh
        indices.hh
        localresidual.hh
        model.hh
        properties.hh
        propertydefaults.hh
        volumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/porousmediumflow/1p2cadsorption/implicit)
