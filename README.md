This file has been created automatically. Please adapt it to your needs.

===============================

The content of this DUNE module was extracted from the module dumux.
In particular, the following subfolders of dumux have been extracted:

  test/porousmediumflow/1p2cadsorption/implicit/,

Additionally, all headers in dumux that are required to build the
executables from the sources

  test/porousmediumflow/1p2cadsorption/implicit/test_cc1p2cadsorption.cc,
  test/porousmediumflow/1p2cadsorption/implicit/test_box1p2cadsorption.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.




